﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace D302
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[]
            {
                new Student("Fred", 3.0, 19),
                new Student("Robert", 2.9, 18),
                new Student("William", 3.1, 20),
                new Student("Pinky", 2.0, 12),
                new Student("Brain", 4.0, 60),
                new Student("Tweety Bird", 3.5, 6),
                new Student("Coyote", 1.0, 40),
            };

            // Print out all of the students
            foreach (Student s in students)
            {
                Console.WriteLine(s.ToString());
            }
            Console.WriteLine();

            // Show some statistics about the students
            Console.WriteLine("Number of Students: {0}", students.Count());
            Console.WriteLine();
            Console.WriteLine("First: {0}", students.FirstOrDefault());
            Console.WriteLine("Last: {0}", students.LastOrDefault());
            Console.WriteLine();
            Console.WriteLine("Sum GPA: {0:F2}", students.Sum(s => s.GPA));
            Console.WriteLine("Average GPA: {0:F2}", students.Average(s => s.GPA));
            Console.WriteLine("Min GPA: {0:F2}", students.Min(s => s.GPA));
            Console.WriteLine("Max GPA: {0:F2}", students.Max(s => s.GPA));
            Console.WriteLine();
            Console.WriteLine("Sum Age: {0}", students.Sum(s => s.Age));
            Console.WriteLine("Average Age: {0:F2}", students.Average(s => s.Age));
            Console.WriteLine("Min Age: {0}", students.Min(s => s.Age));
            Console.WriteLine("Max Age: {0}", students.Max(s => s.Age));
        }
    }
}
