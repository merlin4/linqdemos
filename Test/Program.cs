﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[]
            {
                new Student("Fred", 3.0, 19),
                new Student("Robert", 2.9, 18),
                new Student("William", 3.1, 20),
                new Student("Pinky", 2.0, 12),
                new Student("Brain", 4.0, 60),
                new Student("Tweety Bird", 3.5, 6),
                new Student("Coyote", 1.0, 40),
            };

            var map = new Dictionary<string, Student>();
            foreach (Student student in students)
            {
                map[student.Name] = student;
            }

            for (;;)
            {
                Console.WriteLine();
                Console.WriteLine("What is the student's name?");
                string name = Console.ReadLine();
                Student student = map[name];
                Console.WriteLine(student);
            }

            //string filename = "students.json";
            //try
            //{
            //    using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            //    {
            //        using (var writer = new StreamWriter(stream))
            //        {
            //            var serializer = new JavaScriptSerializer();
            //            string json = serializer.Serialize(students);
            //            writer.Write(json);
            //            writer.Flush();
            //        }
            //    }
            //}
            //catch (IOException ex)
            //{
            //    Console.WriteLine(ex);
            //}

            //IEnumerable<Student> query =
            //    from s in students
            //    where s.Name == "Coyote" || s.GPA == 4.0
            //    select s;

            //foreach (Student s in query)
            //{
            //    Console.WriteLine(s);
            //}
        }
    }
}
