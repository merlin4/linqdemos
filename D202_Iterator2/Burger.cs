﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Burger : IEnumerable<string>
{
    private string[] _toppings;
        
    public Burger(params string[] toppings)
    {
        _toppings = toppings;
    }
        
    public IEnumerator<string> GetEnumerator()
    {
        for (int i = 0; i < _toppings.Length; ++i)
        {
            yield return _toppings[i];
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
