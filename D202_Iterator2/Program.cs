﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D202_Iterator2
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayBurgerToppings(new Burger("Onions", "Pickles"));
            DisplayBurgerToppings(new Burger("Onions", "Pickles", "Cheese"));
            DisplayBurgerToppings(new Burger("Ham", "Bacon", "Pineapple"));
            DisplayBurgerToppings(new Burger("Everything"));
        }

        static void DisplayBurgerToppings(Burger burger)
        {
            Console.WriteLine("Burger w/");
            foreach (string topping in burger)
            {
                Console.Write(topping + ", ");
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
