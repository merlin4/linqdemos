﻿namespace D2_CardShuffle
{
    public enum CardSuit
    {
        HEART = 0, DIAMOND = 1, SPADE = 2, CLUB = 3
    }
}
