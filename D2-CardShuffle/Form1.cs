﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D2_CardShuffle
{
    public partial class Form1 : Form
    {
        private const int BUTTON_WIDTH = 100;
        private const int BUTTON_HEIGHT = 30;
        private const int BUTTON_PAD = 5;

        private Random _random;
        private Button[] _buttons;

        public Form1()
        {
            InitializeComponent();
            this.SuspendLayout();

            outputLabel.Text = "";

            _random = new Random();
            _buttons = new Button[52];

            int index = 0;
            foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)))
            {
                string suitName = "";
                Color color = Color.Black;
                switch (suit)
                {
                    case CardSuit.HEART:
                        suitName = "Heart";
                        color = Color.Red;
                        break;
                    case CardSuit.DIAMOND:
                        suitName = "Diamond";
                        color = Color.Red;
                        break;
                    case CardSuit.SPADE:
                        suitName = "Spade";
                        color = Color.Black;
                        break;
                    case CardSuit.CLUB:
                        suitName = "Club";
                        color = Color.Black;
                        break;
                }

                for (int num = 1; num <= 13; ++num)
                {
                    Button button = new Button()
                    {
                        Name = string.Format("cardButton{0}", index + 1),
                        Text = string.Format("{0} {1}", num, suitName),
                        AutoSize = false,
                        Location = new Point(BUTTON_PAD, BUTTON_PAD),
                        Size = new Size(BUTTON_WIDTH, BUTTON_HEIGHT),
                        TabIndex = 3 + index,
                        BackColor = Color.White,
                        ForeColor = color,
                    };
                    button.Click += cardButton_Click;

                    _buttons[index] = button;
                    this.Controls.Add(button);
                    ++index;
                }
            }

            LayoutCards();

            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private void cardButton_Click(object sender, EventArgs e)
        {
            Button button = ((Button)sender);
            outputLabel.Text = button.Text;
            outputLabel.BackColor = button.BackColor;
            outputLabel.ForeColor = button.ForeColor;
        }

        private void shuffleButton_Click(object sender, EventArgs e)
        {
            _buttons.Shuffle();
            LayoutCards();
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            LayoutCards();
        }

        private void LayoutCards()
        {
            int width = this.Width;
            int height = this.Height;
            int x = BUTTON_PAD;
            int y = BUTTON_PAD;

            for (int index = 0; index < _buttons.Length; ++index)
            {
                Button button = _buttons[index];
                button.AutoSize = false;
                button.Location = new Point(x, y);
                button.Size = new Size(BUTTON_WIDTH, BUTTON_HEIGHT);

                x += (BUTTON_WIDTH + BUTTON_PAD);
                if ((x + BUTTON_WIDTH) >= width)
                {
                    x = BUTTON_PAD;
                    y += (BUTTON_HEIGHT + BUTTON_PAD);
                }
            }

            // Position the shuffle button below the all of the cards
            x = BUTTON_PAD;
            y += 2 * (BUTTON_HEIGHT + BUTTON_PAD);
            shuffleButton.AutoSize = false;
            shuffleButton.Location = new Point(x, y);
            shuffleButton.Size = new Size(BUTTON_WIDTH, BUTTON_HEIGHT);

            // Position the output label next to the shuffle button
            x += (BUTTON_WIDTH + BUTTON_PAD);
            outputLabel.AutoSize = false;
            outputLabel.Location = new Point(x, y);
            outputLabel.Size = new Size(BUTTON_WIDTH, BUTTON_HEIGHT);
        }
    }
}
