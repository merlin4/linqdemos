﻿using System;
using System.Windows.Forms;

namespace D2_CardShuffle
{
    public static class Utils
    {
        public static void Shuffle(this Button[] buttons)
        {
            Random rand = new Random();
            int count = buttons.Length;

            for (int i = 0; i < count; ++i)
            {
                int j = rand.Next(i, count);
                // Swap positions i and j
                Button temp = buttons[i];
                buttons[i] = buttons[j];
                buttons[j] = temp;
            }
        }
    }
}
