﻿using System;
using System.Collections.Generic;

namespace D203_List1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();

            // Add a bunch of students 
            students.AddRange(
                new Student[]
                {
                    new Student("Fred", 3.0, 19),
                    new Student("Robert", 2.9, 18),
                    new Student("William", 3.1, 20),
                    new Student("Pinky", 2.0, 12),
                    new Student("Brain", 4.0, 60),
                    new Student("Tweety Bird", 3.5, 6),
                    new Student("Coyote", 1.0, 40),
                }
            );

            //students.Add(new Student("Freddie", 3.1, 20));

            for (;;)
            {
                // Show the menu
                Console.WriteLine();
                Console.WriteLine("What do you want to do?");
                Console.WriteLine("1. List all students");
                Console.WriteLine("2. Add a student");
                Console.WriteLine("3. Remove a student");
                Console.WriteLine("4. Exit");
                Console.WriteLine("------------------");
                string input = Console.ReadLine();

                Console.WriteLine();
                switch (input)
                {
                    // 1. List all students
                    case "1":
                        {
                            foreach (Student student in students)
                            {
                                Console.WriteLine(student);
                            }
                            break;
                        }
                    // 2. Add a student
                    case "2":
                        {
                            try
                            {
                                Console.WriteLine("Please enter their name");
                                string name = Console.ReadLine();
                                Console.WriteLine("Please enter their gpa");
                                double gpa = double.Parse(Console.ReadLine());
                                Console.WriteLine("Please enter their age");
                                int age = int.Parse(Console.ReadLine());

                                students.Add(new Student(name, gpa, age));
                                Console.WriteLine("Added");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                            break;
                        }
                    // 3. Remove a student
                    case "3":
                        {
                            try
                            {
                                Console.WriteLine("Which student do you want to remove?");
                                for (int i = 0; i < students.Count; ++i)
                                {
                                    Console.WriteLine("{0}. {1}", i, students[i]);
                                }

                                int index = int.Parse(Console.ReadLine());
                                students.RemoveAt(index);
                                Console.WriteLine("Removed");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                            break;
                        }
                    // 4. Exit
                    case "4":
                        Console.WriteLine("DONE");
                        return;
                    // INVALID OPTION
                    default:
                        Console.WriteLine("ERROR: Invalid Option");
                        break;
                }
            }
        }
    }
}
