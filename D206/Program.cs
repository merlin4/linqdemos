﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D206
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[]
            {
                new Student("Fred", 3.0, 19),
                new Student("Robert", 2.9, 18),
                new Student("William", 3.1, 20),
                new Student("Pinky", 2.0, 12),
                new Student("Brain", 4.0, 60),
                new Student("Tweety Bird", 3.5, 6),
                new Student("Coyote", 1.0, 40),
            };

            var map = new Dictionary<string, Student>();
            foreach (Student student in students)
            {
                map[student.Name] = student;
            }

            for (;;)
            {
                Console.WriteLine();
                Console.WriteLine("What is the student's name?");
                string name = Console.ReadLine();
                if (map.TryGetValue(name, out Student student))
                {
                    Console.WriteLine(student);
                }
                else
                {
                    Console.WriteLine("ERROR: Student not found");
                }
            }
        }
    }
}
