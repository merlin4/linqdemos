﻿using System;

namespace D1_NameShuffle
{
    public static class Utils
    {
        public static void Shuffle(this string[] names)
        {
            Random rand = new Random();
            int count = names.Length;

            for (int i = 0; i < count; ++i)
            {
                int j = rand.Next(i, count);
                // Swap positions i and j
                string temp = names[i];
                names[i] = names[j];
                names[j] = temp;
            }
        }
    }
}
