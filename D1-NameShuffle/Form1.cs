﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D1_NameShuffle
{
    public partial class Form1 : Form
    {
        private const int LABEL_WIDTH = 100;
        private const int LABEL_HEIGHT = 25;
        private const int LABEL_PAD = 5;

        private Random _random;
        private string[] _names;
        private Label[] _nameLabels;

        public Form1()
        {
            InitializeComponent();
            this.SuspendLayout();

            _random = new Random();
            _names = new string[]
            {
                "Fred",
                "Robert",
                "William",
                "Pinky",
                "Brain",
                "Tweety Bird",
                "Coyote",
            };
            
            // Create labels for each name and add them to the form
            int count = _names.Length;
            int y = LABEL_PAD;
            _nameLabels = new Label[count];
            for (int i = 0; i < count; ++i)
            {
                Label label = new Label()
                {
                    Name = string.Format("nameLabel{0}", i + 1),
                    Text = _names[i],
                    AutoSize = false,
                    Location = new Point(LABEL_PAD, y),
                    Size = new Size(LABEL_WIDTH, LABEL_HEIGHT),
                    TabIndex = 2 + i,
                };

                y += (LABEL_HEIGHT + LABEL_PAD);
                _nameLabels[i] = label;
                this.Controls.Add(label);
            }

            // Position the shuffle button below the labels
            shuffleButton.AutoSize = false;
            shuffleButton.Location = new Point(LABEL_PAD, y);
            shuffleButton.Size = new Size(LABEL_WIDTH, LABEL_HEIGHT);

            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private void shuffleButton_Click(object sender, EventArgs e)
        {
            _names.Shuffle();

            int count = _names.Length;
            for (int i = 0; i < count; ++i)
            {
                _nameLabels[i].Text = _names[i];
            }
        }
    }
}
