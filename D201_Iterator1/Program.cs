﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D201_Iterator1
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (int x in Multiples(10))
            {
                Console.WriteLine(x);
                Console.ReadLine();
            }
        }

        static IEnumerable<int> Multiples(int n)
        {
            for (int i = 1; i <= n; ++i)
            {
                yield return i * 3;
            }
        }

        static IEnumerable<int> Factorial(int n)
        {
            int product = 1;
            for (int i = 1; i <= n; ++i)
            {
                product *= i;
                yield return product;
            }
        }
    }
}
