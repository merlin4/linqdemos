﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace D301
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[]
            {
                new Student("Fred", 3.0, 19),
                new Student("Robert", 2.9, 18),
                new Student("William", 3.1, 20),
                new Student("Pinky", 2.0, 12),
                new Student("Brain", 4.0, 60),
                new Student("Tweety Bird", 3.5, 6),
                new Student("Coyote", 1.0, 40),
            };

            // Print out all of the students
            foreach (Student s in students)
            {
                Console.WriteLine(s.ToString());
            }
            Console.WriteLine();

            // Search for students that match our search criteria
            IEnumerable<Student> query =
                from s in students
                where s.Name == "Coyote" || s.GPA >= 3.5
                select s;

            Console.WriteLine("Results");
            foreach (Student s in query)
            {
                Console.WriteLine(s);
            }
        }
    }
}
