﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D4_GenericFind
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] integers = new int[]
            {
                4, 5, 7, 9, 3, 42
            };
            double[] doubles = new double[]
            {
                4.3, 3.7, 7.2, 9.8, 3.5, 33.7
            };
            string[] strings = new string[]
            {
                "Fred", "Robert", "William", "Pinky", "Brain", "Tweety Bird", "Coyote"
            };

            Console.WriteLine(
                "FindInt(integers, 7) = {0}", 
                FindInt(integers, 7)
            );
            Console.WriteLine(
                "FindGeneric(integers, 7) = {0}",
                FindGeneric(integers, 7)
            );
            Console.WriteLine(
                "FindGeneric(doubles, 9.8) = {0}",
                FindGeneric(doubles, 9.8)
            );
            Console.WriteLine(
                "FindGeneric(strings, \"Tweety Bird\") = {0}",
                FindGeneric(strings, "Tweety Bird")
            );
            Console.WriteLine(
                "FindGeneric(strings, \"Mickey\") = {0}",
                FindGeneric(strings, "Mickey")
            );
        }

        static int FindInt(int[] arr, int search)
        {
            for (int index = 0; index < arr.Length; ++index)
            {
                if (arr[index] == search)
                {
                    return index;
                }
            }
            return -1;
        }

        static int FindGeneric<T>(T[] arr, T search)
        {
            for (int index = 0; index < arr.Length; ++index)
            {
                if (arr[index].Equals(search))
                {
                    return index;
                }
            }
            return -1;
        }
    }
}
