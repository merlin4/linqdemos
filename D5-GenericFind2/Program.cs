﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5_GenericFind2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] integers = new int[]
            {
                4, 5, 7, 9, 3, 42
            };
            double[] doubles = new double[]
            {
                4.3, 3.7, 7.2, 9.8, 3.5, 33.7
            };
            string[] strings = new string[]
            {
                "Fred", "Robert", "William", "Pinky", "Brain", "Tweety Bird", "Coyote"
            };
            Student[] students = new Student[]
            {
                new Student("Fred", 3.0, 19),
                new Student("Robert", 2.9, 18),
                new Student("William", 3.1, 20),
                new Student("Pinky", 2.0, 12),
                new Student("Brain", 4.0, 60),
                new Student("Tweety Bird", 3.5, 6),
                new Student("Coyote", 1.0, 40),
            };

            Console.WriteLine("enter name");
            string name = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine(
                FindElement(students, x => x.Name.StartsWith(name))
            );

            //Console.WriteLine("enter gpa");
            //double gpa = double.Parse(Console.ReadLine());

            //Console.WriteLine("enter age");
            //double age = double.Parse(Console.ReadLine());

            //Console.WriteLine();
            //Console.WriteLine(
            //    FindElement(students, x => (x.GPA >= gpa && x.Age >= age))
            //);

            //Console.WriteLine(
            //    "FindIndex(integers, 7) = {0}",
            //    FindIndex(integers, 7)
            //);
            //Console.WriteLine(
            //    "FindIndex(doubles, 9.8) = {0}",
            //    FindIndex(doubles, 9.8)
            //);
            //Console.WriteLine(
            //    "FindIndex(strings, \"Tweety Bird\") = {0}",
            //    FindIndex(strings, "Tweety Bird")
            //);
            //Console.WriteLine(
            //    "FindIndex(students, \"Coyote\") = {0}",
            //    FindIndex(students, new Student("Coyote", 0, 0))
            //);
            //Console.WriteLine();


            //Console.WriteLine(
            //    "FindElement(strings, \"Tweety Bird\") = {0}",
            //    FindElement(strings, x => x == "Tweety Bird")
            //);
            //Console.WriteLine(
            //    "FindElement(students, Name == \"Tweety Bird\") = {0}",
            //    FindElement(students, Tweety)
            //);
            //Console.WriteLine(
            //    "FindElement(students, Name == \"Tweety Bird\") = {0}",
            //    FindElement(students, x => x.Name == "Tweety Bird")
            //);
            //Console.WriteLine(
            //    "FindElement(students, GPA >= 3.0) = {0}",
            //    FindElement(students, x => x.GPA >= 3.0)
            //);
            //Console.WriteLine(
            //    "FindElement(students, Age >= 25) = {0}",
            //    FindElement(students, x => x.Age >= 25)
            //);
        }

        static bool Tweety(Student x)
        {
            return x.Name == "Tweety Bird";
        }

        static int FindIndex<T>(T[] arr, T search)
        {
            for (int index = 0; index < arr.Length; ++index)
            {
                if (arr[index].Equals(search))
                {
                    return index;
                }
            }
            return -1;
        }

        delegate bool FindClause<T>(T value);

        static T FindElement<T>(T[] arr, FindClause<T> clause)
        {
            for (int index = 0; index < arr.Length; ++index)
            {
                if (clause(arr[index]))
                {
                    return arr[index];
                }
            }
            return default(T);
        }
    }
}
