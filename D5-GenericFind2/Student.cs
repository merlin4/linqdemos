﻿using System;

public class Student : IComparable
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public double GPA { get; set; }
    public int Age { get; set; }

    public Student(string name, double gpa, int age)
    {
        this.Id = Guid.NewGuid();
        this.Name = name;
        this.GPA = gpa;
        this.Age = age;
    }

    public Student(string name) : this(name, 0.0, 0) 
    {
    }

    public override string ToString()
    {
        return string.Format("(name:{0}, gpa:{1:F2}, age:{2})", Name, GPA, Age);
    }

    public int CompareTo(object obj)
    {
        Student other = ((Student)obj);
        return this.Name.CompareTo(other.Name);
    }

    public override bool Equals(object obj)
    {
        if (obj == null) { return false; }
        if (obj.GetType() != this.GetType()) { return false; }

        Student other = ((Student)obj);
        return this.Name == other.Name; //&& this.GPA == other.GPA;
    }

    public override int GetHashCode()
    {
        return Name.GetHashCode();
    }
}
