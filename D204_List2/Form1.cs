﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace D204_List2
{
    public partial class Form1 : Form
    {
        private List<Student> _students;

        public Form1()
        {
            InitializeComponent();

            // Prepopulate the student list
            _students = new List<Student>();
            _students.AddRange(
                new Student[]
                {
                    new Student("Fred", 3.0, 19),
                    new Student("Robert", 2.9, 18),
                    new Student("William", 3.1, 20),
                    new Student("Pinky", 2.0, 12),
                    new Student("Brain", 4.0, 60),
                    new Student("Tweety Bird", 3.5, 6),
                    new Student("Coyote", 1.0, 40),
                }
            );
            RefreshStudentListBox();
        }

        private void RefreshStudentListBox()
        {
            studentsListBox.Items.Clear();
            studentsListBox.Items.AddRange(_students.ToArray());
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                string name = nameBox.Text;
                double gpa = double.Parse(gpaBox.Text);
                int age = int.Parse(ageBox.Text);

                _students.Add(new Student(name, gpa, age));
                RefreshStudentListBox();

                MessageBox.Show(
                    owner: this,
                    caption: "Added",
                    text: "Added",
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    owner: this,
                    caption: ex.GetType().Name,
                    text: ex.ToString(),
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Error
                );
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Student student = (Student)studentsListBox.SelectedItem;
                if (student != null)
                {
                    _students.Remove(student);
                    RefreshStudentListBox();

                    MessageBox.Show(
                        owner: this,
                        caption: "Removed",
                        text: "Removed",
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    owner: this,
                    caption: ex.GetType().Name,
                    text: ex.ToString(),
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Error
                );
            }
        }
    }
}
