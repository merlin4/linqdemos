﻿using System;

namespace D3_GenericShuffle
{
    public static class Utils
    {
        public static void Shuffle<T>(this T[] arr)
        {
            Random rand = new Random();
            int count = arr.Length;

            for (int i = 0; i < count; ++i)
            {
                int j = rand.Next(i, count);
                // Swap positions i and j
                T temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}
