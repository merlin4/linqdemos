﻿namespace D3_GenericShuffle
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shuffleButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.choclateCheck = new System.Windows.Forms.CheckBox();
            this.sugarCheck = new System.Windows.Forms.CheckBox();
            this.oatmealCheck = new System.Windows.Forms.CheckBox();
            this.appleRadio = new System.Windows.Forms.RadioButton();
            this.pearRadio = new System.Windows.Forms.RadioButton();
            this.orangeRadio = new System.Windows.Forms.RadioButton();
            this.mangoRadio = new System.Windows.Forms.RadioButton();
            this.pintoButton = new System.Windows.Forms.Button();
            this.redButton = new System.Windows.Forms.Button();
            this.garbanzoButton = new System.Windows.Forms.Button();
            this.kidneyButton = new System.Windows.Forms.Button();
            this.limaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // shuffleButton
            // 
            this.shuffleButton.Location = new System.Drawing.Point(23, 283);
            this.shuffleButton.Name = "shuffleButton";
            this.shuffleButton.Size = new System.Drawing.Size(75, 23);
            this.shuffleButton.TabIndex = 3;
            this.shuffleButton.Text = "Shuffle";
            this.shuffleButton.UseVisualStyleBackColor = true;
            this.shuffleButton.Click += new System.EventHandler(this.shuffleButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cookies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Fruits";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Beans";
            // 
            // choclateCheck
            // 
            this.choclateCheck.AutoSize = true;
            this.choclateCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.choclateCheck.ForeColor = System.Drawing.Color.White;
            this.choclateCheck.Location = new System.Drawing.Point(15, 41);
            this.choclateCheck.Name = "choclateCheck";
            this.choclateCheck.Size = new System.Drawing.Size(117, 21);
            this.choclateCheck.TabIndex = 7;
            this.choclateCheck.Text = "Choclate Chip";
            this.choclateCheck.UseVisualStyleBackColor = false;
            // 
            // sugarCheck
            // 
            this.sugarCheck.AutoSize = true;
            this.sugarCheck.BackColor = System.Drawing.Color.White;
            this.sugarCheck.Location = new System.Drawing.Point(15, 68);
            this.sugarCheck.Name = "sugarCheck";
            this.sugarCheck.Size = new System.Drawing.Size(68, 21);
            this.sugarCheck.TabIndex = 8;
            this.sugarCheck.Text = "Sugar";
            this.sugarCheck.UseVisualStyleBackColor = false;
            // 
            // oatmealCheck
            // 
            this.oatmealCheck.AutoSize = true;
            this.oatmealCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.oatmealCheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.oatmealCheck.Location = new System.Drawing.Point(15, 95);
            this.oatmealCheck.Name = "oatmealCheck";
            this.oatmealCheck.Size = new System.Drawing.Size(126, 21);
            this.oatmealCheck.TabIndex = 9;
            this.oatmealCheck.Text = "Oatmeal Raisin";
            this.oatmealCheck.UseVisualStyleBackColor = false;
            // 
            // appleRadio
            // 
            this.appleRadio.AutoSize = true;
            this.appleRadio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.appleRadio.Location = new System.Drawing.Point(177, 41);
            this.appleRadio.Name = "appleRadio";
            this.appleRadio.Size = new System.Drawing.Size(65, 21);
            this.appleRadio.TabIndex = 10;
            this.appleRadio.TabStop = true;
            this.appleRadio.Text = "Apple";
            this.appleRadio.UseVisualStyleBackColor = false;
            // 
            // pearRadio
            // 
            this.pearRadio.AutoSize = true;
            this.pearRadio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pearRadio.Location = new System.Drawing.Point(177, 67);
            this.pearRadio.Name = "pearRadio";
            this.pearRadio.Size = new System.Drawing.Size(59, 21);
            this.pearRadio.TabIndex = 11;
            this.pearRadio.TabStop = true;
            this.pearRadio.Text = "Pear";
            this.pearRadio.UseVisualStyleBackColor = false;
            // 
            // orangeRadio
            // 
            this.orangeRadio.AutoSize = true;
            this.orangeRadio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.orangeRadio.Location = new System.Drawing.Point(177, 95);
            this.orangeRadio.Name = "orangeRadio";
            this.orangeRadio.Size = new System.Drawing.Size(77, 21);
            this.orangeRadio.TabIndex = 12;
            this.orangeRadio.TabStop = true;
            this.orangeRadio.Text = "Orange";
            this.orangeRadio.UseVisualStyleBackColor = false;
            // 
            // mangoRadio
            // 
            this.mangoRadio.AutoSize = true;
            this.mangoRadio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mangoRadio.Location = new System.Drawing.Point(177, 122);
            this.mangoRadio.Name = "mangoRadio";
            this.mangoRadio.Size = new System.Drawing.Size(72, 21);
            this.mangoRadio.TabIndex = 18;
            this.mangoRadio.TabStop = true;
            this.mangoRadio.Text = "Mango";
            this.mangoRadio.UseVisualStyleBackColor = false;
            // 
            // pintoButton
            // 
            this.pintoButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.pintoButton.Location = new System.Drawing.Point(335, 41);
            this.pintoButton.Name = "pintoButton";
            this.pintoButton.Size = new System.Drawing.Size(152, 23);
            this.pintoButton.TabIndex = 19;
            this.pintoButton.Text = "Pinto Beans";
            this.pintoButton.UseVisualStyleBackColor = false;
            // 
            // redButton
            // 
            this.redButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.redButton.Location = new System.Drawing.Point(335, 70);
            this.redButton.Name = "redButton";
            this.redButton.Size = new System.Drawing.Size(152, 23);
            this.redButton.TabIndex = 20;
            this.redButton.Text = "Red Beans";
            this.redButton.UseVisualStyleBackColor = false;
            // 
            // garbanzoButton
            // 
            this.garbanzoButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.garbanzoButton.Location = new System.Drawing.Point(335, 99);
            this.garbanzoButton.Name = "garbanzoButton";
            this.garbanzoButton.Size = new System.Drawing.Size(152, 23);
            this.garbanzoButton.TabIndex = 21;
            this.garbanzoButton.Text = "Garbanzo Beans";
            this.garbanzoButton.UseVisualStyleBackColor = false;
            // 
            // kidneyButton
            // 
            this.kidneyButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.kidneyButton.Location = new System.Drawing.Point(335, 128);
            this.kidneyButton.Name = "kidneyButton";
            this.kidneyButton.Size = new System.Drawing.Size(152, 23);
            this.kidneyButton.TabIndex = 22;
            this.kidneyButton.Text = "Kidney Beans";
            this.kidneyButton.UseVisualStyleBackColor = false;
            // 
            // limaButton
            // 
            this.limaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.limaButton.Location = new System.Drawing.Point(335, 157);
            this.limaButton.Name = "limaButton";
            this.limaButton.Size = new System.Drawing.Size(152, 23);
            this.limaButton.TabIndex = 23;
            this.limaButton.Text = "Lima Beans";
            this.limaButton.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 473);
            this.Controls.Add(this.limaButton);
            this.Controls.Add(this.kidneyButton);
            this.Controls.Add(this.garbanzoButton);
            this.Controls.Add(this.redButton);
            this.Controls.Add(this.pintoButton);
            this.Controls.Add(this.mangoRadio);
            this.Controls.Add(this.orangeRadio);
            this.Controls.Add(this.pearRadio);
            this.Controls.Add(this.appleRadio);
            this.Controls.Add(this.oatmealCheck);
            this.Controls.Add(this.sugarCheck);
            this.Controls.Add(this.choclateCheck);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shuffleButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button shuffleButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox choclateCheck;
        private System.Windows.Forms.CheckBox sugarCheck;
        private System.Windows.Forms.CheckBox oatmealCheck;
        private System.Windows.Forms.RadioButton appleRadio;
        private System.Windows.Forms.RadioButton pearRadio;
        private System.Windows.Forms.RadioButton orangeRadio;
        private System.Windows.Forms.RadioButton mangoRadio;
        private System.Windows.Forms.Button pintoButton;
        private System.Windows.Forms.Button redButton;
        private System.Windows.Forms.Button garbanzoButton;
        private System.Windows.Forms.Button kidneyButton;
        private System.Windows.Forms.Button limaButton;
    }
}

