﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3_GenericShuffle
{
    public partial class Form1 : Form
    {
        private Random _random;
        private CheckBox[] _checkBoxes;
        private RadioButton[] _radios;
        private Button[] _buttons;

        private Point[] _checkBoxLocations;
        private Point[] _radioLocations;
        private Point[] _buttonLocations;

        public Form1()
        {
            InitializeComponent();

            // Aggregate the controls into arrays

            _checkBoxes = new CheckBox[]
            {
                choclateCheck,
                sugarCheck,
                oatmealCheck,
            };
            _radios = new RadioButton[]
            {
                appleRadio,
                pearRadio,
                orangeRadio,
                mangoRadio,
            };
            _buttons = new Button[]
            {
                pintoButton,
                redButton,
                garbanzoButton,
                kidneyButton,
                limaButton,
            };

            // Save the starting positions of the controls

            _checkBoxLocations = new Point[_checkBoxes.Length];
            for (int i = 0; i < _checkBoxes.Length; ++i)
            {
                _checkBoxLocations[i] = _checkBoxes[i].Location;
            }

            _radioLocations = new Point[_radios.Length];
            for (int i = 0; i < _radios.Length; ++i)
            {
                _radioLocations[i] = _radios[i].Location;
            }

            _buttonLocations = new Point[_buttons.Length];
            for (int i = 0; i < _buttons.Length; ++i)
            {
                _buttonLocations[i] = _buttons[i].Location;
            }
        }

        private void shuffleButton_Click(object sender, EventArgs e)
        {
            // Shuffle the controls
            Utils.Shuffle<CheckBox>(_checkBoxes);
            Utils.Shuffle<RadioButton>(_radios);
            Utils.Shuffle<Button>(_buttons);

            //_checkBoxes.Shuffle();
            //_radios.Shuffle();
            //_buttons.Shuffle();

            // Reposition the controls
            for (int i = 0; i < _checkBoxes.Length; ++i)
            {
                _checkBoxes[i].Location = _checkBoxLocations[i];
            }
            for (int i = 0; i < _radios.Length; ++i)
            {
                _radios[i].Location = _radioLocations[i];
            }
            for (int i = 0; i < _buttons.Length; ++i)
            {
                _buttons[i].Location = _buttonLocations[i];
            }
        }
    }
}
