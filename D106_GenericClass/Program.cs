﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D106_GenericClass
{
    class Program
    {
        static void Main(string[] args)
        {
            ResizableArray<int> arr = 
                new ResizableArray<int>(10, 1, 2, 3, 4, 5);

            for (;;)
            {
                Console.WriteLine("1. Add Item");
                Console.WriteLine("2. List Items");
                Console.WriteLine("3. Compute Sum");
                Console.WriteLine("------------------");
                string input = Console.ReadLine();

                Console.WriteLine();
                switch (input)
                {
                    case "1":
                        {
                            Console.WriteLine("Enter item to add");
                            input = Console.ReadLine();
                            int num;
                            if (int.TryParse(input, out num))
                            {
                                arr.Add(num);
                                Console.WriteLine("Item added");
                            }
                            else
                            {
                                Console.WriteLine("ERROR: NOT A NUMBER");
                            }
                            break;
                        }
                    case "2":
                        {
                            Console.WriteLine("List");
                            for (int i = 0; i < arr.Count; ++i)
                            {
                                Console.WriteLine(arr.GetElement(i));
                            }
                            break;
                        }
                    case "3":
                        {
                            int sum = 0;
                            for (int i = 0; i < arr.Count; ++i)
                            {
                                sum += arr.GetElement(i);
                            }
                            Console.WriteLine("Sum: {0}", sum);
                            break;
                        }
                    default:
                        Console.WriteLine("ERROR: INVALID OPTION");
                        break;
                }

                Console.WriteLine();
            }
        }
    }
}
