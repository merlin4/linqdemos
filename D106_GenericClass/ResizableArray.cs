﻿using System;

namespace D106_GenericClass
{
    public class ResizableArray<T>
    {
        private T[] _items;
        private int _count;

        public ResizableArray(int size)
        {
            _items = new T[size];
            _count = 0;
        }

        public ResizableArray(int size, params T[] items)
        {
            if (items.Length > size)
            {
                size = items.Length;
            }

            _items = new T[size];
            _count = 0;

            foreach (T item in items)
            {
                _items[_count] = item;
                ++_count;
            }
        }

        public int Count
        {
            get { return _count; }
        }

        public T GetElement(int index)
        {
            if (index >= _count) { throw new IndexOutOfRangeException(); }

            return _items[index];
        }

        public void SetElement(int index, T item)
        {
            if (index >= _count) { throw new IndexOutOfRangeException(); }

            _items[index] = item;
        }

        public void Add(T item)
        {
            // Do we need to make more space first?
            if (_count >= _items.Length)
            {
                Resize(_items.Length * 2);
            }

            // Add the item to the end of the array
            _items[_count] = item;
            // Increment the count
            ++_count;
        }

        public void Remove(int index)
        {
            if (index >= _count) { throw new IndexOutOfRangeException(); }

            // Shift the elements afterwards over one step to fill the gap
            for (int i = index + 1; index < _count; ++i)
            {
                _items[i - 1] = _items[i];
            }
            // Decrement the count
            --_count;
        }

        private void Resize(int capacity)
        {
            // Are we shrinking the array?
            if (capacity < _count) { _count = capacity; }

            // Allocate space for the new array
            T[] newArray = new T[capacity];
            // Copy all of the data into the new array
            for (int i = 0; i < _count; ++i)
            {
                newArray[i] = _items[i];
            }
            // Save the new array
            _items = newArray;
        }
    }
}
